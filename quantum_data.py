import torch
from torch.utils import data
import pandas as pd

class QuantumDataset(data.Dataset):
  'Characterizes a dataset for PyTorch'
  def __init__(self, bad_measurements, good_measurements):
        cuda = True if torch.cuda.is_available() else False
        Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
        'Initialization'
        self.bad_measurements = Tensor(bad_measurements)
        self.good_measurements = Tensor(good_measurements)

  def __len__(self):
        'Denotes the total number of samples'
        return len(self.good_measurements)

  def __getitem__(self, index):
        'Generates one sample of data'
        # Select sample
        bad = self.bad_measurements[index]
        good = self.good_measurements[index]


        return bad, good


if __name__ == "__main__":
    goods = pd.read_csv("./data/setting_a/training/good_training_data_exporting").values
    bads =  pd.read_csv("./data/setting_a/training/bad_training_data_exporting").values

    qd = QuantumDataset(bads, goods)

