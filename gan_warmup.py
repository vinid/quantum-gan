import numpy as np
from torch.utils.data import DataLoader
from torch.autograd import Variable
import torch.nn as nn
import torch
from quantum_data import QuantumDataset
import pandas as pd
import itertools

def evaluate(generator, dataloader):
    """
    Funzione che prende in input il generatore e l'oggetto che contiene il test/validation set e restituisce
    lo score di fidelity
    :param generator:
    :param dataloader:
    :return:
    """
    average_d = []
    for i, (bads, goods) in enumerate(dataloader):

        z = Variable(Tensor(bads))

        gen_imgs = generator(z)

        average_d.append(
            np.sum(np.multiply(np.sqrt(gen_imgs.detach().cpu().numpy()), np.sqrt(goods.cpu().numpy()))) ** 2)

    return {"fidelity": (np.average(average_d))}

# Materiale per CUDA
cuda = True if torch.cuda.is_available() else False
Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

class Generator(nn.Module):
    """
    Generator
    """
    def __init__(self):
        super(Generator, self).__init__()

        def block(in_feat, out_feat, normalize=True):
            layers = [nn.Linear(in_feat, out_feat)]
            if normalize:
                layers.append(nn.BatchNorm1d(out_feat))
            layers.append(nn.LeakyReLU(0.2, inplace=True))
            return layers

        self.model = nn.Sequential(
            *block(36, 400),
            *block(400, 200),
            nn.Linear(200, 36),
            nn.Softmax())

    def forward(self, z):
        img = self.model(z)
        return img


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()

        self.model = nn.Sequential(
            nn.Linear(36, 20),
            nn.Tanh(),
            nn.Dropout(0.2),
            nn.Linear(20, 1),
            nn.Sigmoid(),
        )

    def forward(self, img):
        return self.model(img)

tr_goods = pd.read_csv("./data/setting_a/training/good_training_data_exporting").values
tr_bads = pd.read_csv("./data/setting_a/training/bad_training_data_exporting").values

vl_goods = pd.read_csv("./data/setting_a/valid/good_valid_data_exporting").values
vl_bads = pd.read_csv("./data/setting_a/valid/bad_valid_data_exporting").values

ts_goods = pd.read_csv("./data/setting_a/test/good_test_data_exporting").values
ts_bads = pd.read_csv("./data/setting_a/test/bad_test_data_exporting").values

training = QuantumDataset(tr_bads, tr_goods)
validation = QuantumDataset(vl_bads, vl_goods)
testing = QuantumDataset(ts_bads, ts_goods)

dataloader = torch.utils.data.DataLoader(training, batch_size=1000, shuffle=True)
dataloader_valid = torch.utils.data.DataLoader(validation, batch_size=1, shuffle=True)
dataloader_testing = torch.utils.data.DataLoader(testing, batch_size=1, shuffle=True)


combinations_of_training_epochs = [100, 500, 1000, 1000, 10000]
# faccio training su più combinazioni di epoche
with open("gan_wup_results", "w") as write_results:
    for wme, gae in list(itertools.product(combinations_of_training_epochs, combinations_of_training_epochs)):

        # Variabili Generali
        PATIENCE_FOR_EARLY_STOPPING = 50
        WARM_UP_EPOCHS = wme
        GAN_EPOCHS = gae

        # Loss function
        adversarial_loss = torch.nn.BCELoss()
        generator_loss = torch.nn.KLDivLoss()


        # Configure data loader
        generator = Generator()
        discriminator = Discriminator()


        if cuda:
            generator.cuda()
            discriminator.cuda()
            adversarial_loss.cuda()
            generator_loss.cuda()

        # Optimizers
        optimizer_G = torch.optim.RMSprop(generator.parameters())
        optimizer_D = torch.optim.Adam(discriminator.parameters())

        # ----------
        #  WarmUp Training
        # ----------
        epoch_divider = int(WARM_UP_EPOCHS/5) # serve solo per le print
        for epoch in range(WARM_UP_EPOCHS):
            for i, (bads, goods) in enumerate(dataloader):
                optimizer_G.zero_grad()

                # Sample noise as generator input
                z = Variable(Tensor(bads))

                # Generate a batch of images
                gen_imgs = generator(z)

                loss = generator_loss(gen_imgs.log(), goods)

                loss.backward()
                optimizer_G.step()
                if epoch % epoch_divider == 0 and i == 0:
                    print("[Epoch %d/%d] Warm Up"
                          % (epoch, WARM_UP_EPOCHS))

        generator.eval()
        discriminator.eval()
        # calcolo la fidelity della NN sul testing
        warm_up_fidelity = evaluate(generator, dataloader_testing)["fidelity"]
        generator.train()
        discriminator.train()

        # freezing layers
        for name, param in generator.named_parameters():
            param.requires_grad = False
            if name == "model.6.weight" or name == "model.6.bias":
                continue

        # ----------
        #  Training
        # ----------
        optimizer_G = torch.optim.RMSprop(generator.parameters())


        patience_counter = 0 # per early stopping
        current_validation_fidelity = 0
        epoch_divider = int(GAN_EPOCHS/5)
        stopped_at = 0
        for epoch in range(GAN_EPOCHS):
            for i, (bads, goods) in enumerate(dataloader):

                valid = Variable(Tensor(bads.size(0), 1).fill_(1.0), requires_grad=False)
                fake = Variable(Tensor(bads.size(0), 1).fill_(0.0), requires_grad=False)

                # -----------------
                #  Train Generator
                # -----------------
                for m in range(0, 5): # generator trainato più volte
                    optimizer_G.zero_grad()

                    # Sample noise as generator input
                    z = Variable(Tensor(bads))

                    # Generate a batch of images
                    gen_imgs = generator(z)
                    # print(bads, gen_imgs)
                    # Loss measures generator's ability to fool the discriminator

                    # Combino questa loss con una loss di ricostruzione (KL tra generato e buono)
                    g_loss = adversarial_loss(discriminator(gen_imgs), valid)
                    rec_loss = generator_loss(gen_imgs.log(), goods)

                    b_loss = (g_loss + rec_loss)/2

                    b_loss.backward()
                    optimizer_G.step()

                # ---------------------
                #  Train Discriminator
                # ---------------------

                optimizer_D.zero_grad()

                # Measure discriminator's ability to classify real from generated samples
                real_loss = adversarial_loss(discriminator(goods), valid)

                fake_loss = adversarial_loss(discriminator(gen_imgs.detach()), fake)

                d_loss = (real_loss + fake_loss) / 2

                d_loss.backward()
                optimizer_D.step()

                if epoch % epoch_divider == 0 and i == 0:
                    print("[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f]"
                          % (epoch, GAN_EPOCHS, i, len(dataloader), d_loss.item(), g_loss.item()))

            generator.eval()
            # prendo fidelity sul valid per early stopping
            gan_fidelity_valid = evaluate(generator, dataloader_valid)["fidelity"]
            generator.train()

            if gan_fidelity_valid < current_validation_fidelity:
                # se la validity sta scendendo alzo il counter per lo stoppping
                if patience_counter == PATIENCE_FOR_EARLY_STOPPING:
                    print("EARLY STOPPING AT", epoch)
                    stopped_at = epoch
                    break
                else:
                    patience_counter = patience_counter + 1
            else:
                current_validation_fidelity = gan_fidelity_valid


        generator.eval()
        discriminator.eval()
        gan_fidelity = evaluate(generator, dataloader_testing)["fidelity"]
        # salvo risultati
        write_results.write(str(wme)+ "," + str(gae) + "," +  str(epoch) + "," + str(gan_fidelity) + "," + str(warm_up_fidelity) + "\n")